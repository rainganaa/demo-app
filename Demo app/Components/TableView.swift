//
//  TableView.swift
//  Demo app
//
//  Created by Rain on 2023.06.20.
//

import Foundation
import UIKit

class TableView: UITableView {
    override var contentSize:CGSize {
            didSet {
                invalidateIntrinsicContentSize()
            }
        }
        override var intrinsicContentSize: CGSize {
            layoutIfNeeded()
            return CGSize(width: UIView.noIntrinsicMetric, height: contentSize.height)
        }
}
