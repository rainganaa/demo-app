//
//  BaseController.swift
//  Demo app
//
//  Created by Rain on 2023.06.20.
//
import UIKit
import Foundation
import SPIndicator
class BaseController: UIViewController{
    var api:APIServices!
    public var config:Config!
    enum ImageType:String{
        case thumbnail
        case poster
        case backround
    }
    enum DropAlertType:String{
        case successful
        case unsuccessful
        case info
        case text
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        api = APIServices()
    }
    func getConfig(){
        api.getConfig { response in
            self.config = response
        } failure: { error in
            print(error!)
        }
    }
    func getImageUrl(_ imageType:ImageType) -> String{
        switch imageType {
        case .thumbnail:
            return String("\(self.config.images?.base_url ?? "")w300")
        case .backround:
            return String("\(self.config.images?.base_url ?? "")w185")
        case .poster:
            return String("\(self.config.images?.base_url ?? "")w500")
        }
    }
    public func showDropAlert(title:String?, message:String?, type:DropAlertType, completion: (() -> Void)? = nil){
        let indicatorView = SPIndicatorView(title: title ?? "", message: message)
        indicatorView.dismissByDrag = true
        indicatorView.subtitleLabel?.numberOfLines = 0
        DispatchQueue.main.async {
            switch type {
            case .successful:
                indicatorView.present(haptic: .success) {
                    (completion ?? {})()
                }
            case .unsuccessful:
                indicatorView.present(haptic: .error) {
                    (completion ?? {})()
                }
            case .info:
                indicatorView.present(haptic: .warning) {
                    (completion ?? {})()
                }
            case .text:
                indicatorView.present(haptic: .none) {
                    (completion ?? {})()
                }
            }
            
        }
    }
}
extension UIImageView {
    func load(url: URL ,completion: @escaping () -> (), failure:@escaping () -> ()) {
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        completion()
                        self?.image = image
                    }
                }else{
                    failure()
                }
            }else{
                failure()
            }
        }
    }
    func load(url: URL, placeholder: UIImage?, cache: URLCache? = nil,completion: @escaping () -> (), failure:@escaping () -> ()) {
            let cache = cache ?? URLCache.shared
            let request = URLRequest(url: url)
            if let data = cache.cachedResponse(for: request)?.data, let image = UIImage(data: data) {
                DispatchQueue.main.async {
                    print("image loaded from cache")
                    self.image = image
                }
            } else {
                self.image = placeholder
                URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) in
                    if let data = data, let response = response, ((response as? HTTPURLResponse)?.statusCode ?? 500) < 300, let image = UIImage(data: data) {
                        let cachedData = CachedURLResponse(response: response, data: data)
                        cache.storeCachedResponse(cachedData, for: request)
                        DispatchQueue.main.async {
                            print("image loaded from server")
                            self.image = image
                        }
                    }
                }).resume()
            }
        }
}
