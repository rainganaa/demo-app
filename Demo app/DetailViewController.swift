//
//  DetailViewController.swift
//  Demo app
//
//  Created by Rain on 2023.06.20.
//

import UIKit

class DetailViewController: BaseController {
    var result:Result!
    var isFavorite:Bool!
    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var overviewLabel: UILabel!
    @IBOutlet weak var favoriteButton: UIButton!
    @IBOutlet weak var blurImageView: BlurImageView!
    var didClosed : (() -> ()) = {}
    override func viewDidDisappear(_ animated: Bool) {
        self.didClosed()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setupBlurImageView()
        setupContents()
        updateButtonAppearance()
    }
    func setupBlurImageView(){
        let urlString = self.getImageUrl(.poster).appending((result.poster_path ?? ""))
        blurImageView.imageView.load(url:URL(string: urlString)!, placeholder: nil, cache: URLCache()) {
        } failure: {
        }
    }
    func setupContents(){
        let urlString = self.getImageUrl(.poster).appending((result.poster_path ?? ""))
        posterImageView.load(url:URL(string: urlString)!, placeholder: nil, cache: URLCache()) {
        } failure: {
        }
        titleLabel.text = result.title
        dateLabel.text = result.release_date
        overviewLabel.text = result.overview
    }
    func updateButtonAppearance(){
        favoriteButton.setTitle(isFavorite ? Strings.buttonTitleUnFavorite: Strings.buttonTitleFavorite, for: .normal)
    }
    func setFavorite(_ addFavorite:Bool){
        self.api.addToFavorite(movieId: result.id ?? 0, favorite: addFavorite) { response in
            DispatchQueue.main.async {
                self.favoriteButton.isEnabled = true
                if response.success ?? true{
                    if response.status_code == Constants.FAVORITE_STATUS_CODE_ADDED{
                        self.showDropAlert(title: nil, message: response.status_message, type: .successful)
                        self.isFavorite = true
                    }else{
                        self.showDropAlert(title: nil, message: response.status_message, type: .successful)
                        self.isFavorite = false
                    }
                }else{
                    self.showDropAlert(title: nil, message: response.status_message, type: .unsuccessful)
                }
                self.updateButtonAppearance()
            }
        } failure: { error in
            self.showDropAlert(title: nil, message: error?.localizedDescription , type: .unsuccessful)
        }
    }
    @IBAction func favoriteButtonClicked(_ sender: Any) {
        favoriteButton.isEnabled = false
        setFavorite(!self.isFavorite)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
