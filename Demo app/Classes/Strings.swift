//
//  Strings.swift
//  Demo app
//
//  Created by Rain on 2023.06.20.
//

import Foundation
class Strings{
    static let buttonTitleFavorite = "Add to Favorites"
    static let buttonTitleUnFavorite = "Remove from Favorites"
    
    static let unExpectedError = "Unexpected Error"
}
