//
//  ViewController.swift
//  Demo app
//
//  Created by Rain on 2023.06.20.
//

import UIKit

class ViewController: BaseController{
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    var searchData:Search!
    var selectedResult:Result!
    var favoriteData:Search!
    var shouldLoad = false
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        getConfig()
        getFavoriteData(page: 1)
    }
    func search(text:String?, page:Int){
        self.api.search(query: text, page: page) { response in
            let tempData = response
            self.shouldLoad = tempData.page ?? 0 < tempData.total_pages ?? 0 ? true:false
            if(self.searchData == nil){
                self.searchData = tempData
            }else{
                self.searchData.results? += tempData.results!
                self.searchData.page = tempData.page
            }
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        } failure: { error in
            self.shouldLoad = false
            self.showDropAlert(title: nil, message: Strings.unExpectedError, type: .unsuccessful)
        }
    }
    func getFavoriteData(page:Int){
        self.api.getFavorites(page: page) { response in
            self.favoriteData = response
        } failure: { error in
            
        }
    }
    func checkFavorite(movieId:Int?) -> Bool{
        let contain = (self.favoriteData.results?.contains(where: { result in
            return result.id == movieId
        }))!
        return contain
    }
    @IBAction func favoriteButtonClicked(_ sender: Any) {
        self.performSegue(withIdentifier: "segueToFavorite", sender: self)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueToDetail"{
            let detailViewController = segue.destination as! DetailViewController
            detailViewController.result = self.selectedResult
            detailViewController.config = self.config
            detailViewController.isFavorite = self.checkFavorite(movieId: selectedResult.id)
            detailViewController.didClosed = {
                DispatchQueue.main.async {
                    self.getFavoriteData(page: 1)
                }
            }
        }else{
            let favoriteViewController = segue.destination as! FavoriteViewController
            favoriteViewController.config = self.config
        }
    }
}
extension ViewController: UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate, UISearchBarDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.searchData == nil { return 0}
        return self.searchData.results?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "searchCell", for: indexPath) as? TableViewCell{
            if self.searchData.results?.count ?? 0 > indexPath.row{
                let data = self.searchData.results?[indexPath.row]
                cell.titleLabel.text = data?.title ?? ""
                cell.descLabel.text = data?.release_date ?? ""
                DispatchQueue.main.async {
                    cell.iconImageView.image = nil
                    let urlString = self.getImageUrl(.thumbnail).appending((data?.backdrop_path ?? ""))
                    cell.iconImageView.load(url:URL(string: urlString)!, placeholder: nil, cache: URLCache()) {
                    } failure: {
                        
                    }
                }
            }
            return cell
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedResult = self.searchData.results?[indexPath.row]
        searchBar.endEditing(true)
        performSegue(withIdentifier: "segueToDetail", sender: self)
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let height = self.tableView.frame.size.height
        let contentYoffset = scrollView.contentOffset.y
        let distanceFromBottom = self.tableView.contentSize.height - contentYoffset
        if distanceFromBottom < height {
            if self.shouldLoad{
                self.shouldLoad = false
                self.search(text: searchBar.text, page: self.searchData.page! + 1)
            }
        }
    }
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        searchBar.endEditing(true)
    }
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        return true
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchData = nil
        self.search(text: searchText, page: 1)
    }
}
