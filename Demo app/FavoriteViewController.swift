//
//  FavoriteViewController.swift
//  Demo app
//
//  Created by Rain on 2023.06.20.
//

import UIKit

class FavoriteViewController: BaseController {
    @IBOutlet weak var tableView: UITableView!
    var selectedResult:Result!
    var favoriteData:Search!
    var shouldLoad = false

    override func viewDidLoad() {
        super.viewDidLoad()
        getFavoriteData(page: 1)
        // Do any additional setup after loading the view.
    }
    func getFavoriteData(page:Int){
        self.api.getFavorites(page: page) { response in
            let tempData = response
            self.shouldLoad = tempData.page ?? 0 < tempData.total_pages ?? 0 ? true:false
            if(self.favoriteData == nil){
                self.favoriteData = tempData
            }else{
                self.favoriteData.results? += tempData.results!
                self.favoriteData.page = tempData.page
            }
            DispatchQueue.main.async {
                self.tableView.allowsSelection = true
                self.tableView.reloadData()
            }
        } failure: { error in
            self.shouldLoad = false
            self.showDropAlert(title: nil, message: Strings.unExpectedError, type: .unsuccessful)
        }
    }
    func checkFavorite(movieId:Int?) -> Bool{
        let contain = (self.favoriteData.results?.contains(where: { result in
            return result.id == movieId
        }))!
        return contain
    }
    @IBAction func backButtonClicked(_ sender: Any) {
        self.dismiss(animated: true)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let detailViewController = segue.destination as! DetailViewController
        detailViewController.result = self.selectedResult
        detailViewController.config = self.config
        detailViewController.isFavorite = self.checkFavorite(movieId: selectedResult.id)
        detailViewController.didClosed = {
            DispatchQueue.main.async {
                self.tableView.allowsSelection = false
                self.favoriteData = nil
                self.getFavoriteData(page: 1)
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
}
extension FavoriteViewController: UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate, UISearchBarDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.favoriteData == nil { return 0}
        return self.favoriteData.results?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "searchCell", for: indexPath) as? TableViewCell{
            if self.favoriteData.results?.count ?? 0 > indexPath.row{
                let data = self.favoriteData.results?[indexPath.row]
                cell.titleLabel.text = data?.title ?? ""
                cell.descLabel.text = data?.release_date ?? ""
                cell.iconImageView.image = nil
                
                let urlString = self.getImageUrl(.thumbnail).appending((data?.backdrop_path ?? ""))
                print("image url:\(urlString)")
                cell.iconImageView.load(url:URL(string: urlString)!, placeholder: nil, cache: URLCache()) {
                } failure: {
                }
            }
            return cell
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedResult = self.favoriteData.results?[indexPath.row]
        performSegue(withIdentifier: "segueFavoriteToDetail", sender: self)
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let height = self.tableView.frame.size.height
        let contentYoffset = scrollView.contentOffset.y
        let distanceFromBottom = self.tableView.contentSize.height - contentYoffset
        if distanceFromBottom < height {
            if self.shouldLoad{
                self.shouldLoad = false
                self.getFavoriteData(page: self.favoriteData.page! + 1)
            }
        }
    }
}
